let gulp = require('gulp'), // Подключаем Gulp
    cleanCSS = require('gulp-clean-css'),
    sass = require('gulp-sass'), // Подключаем Sass пакет
    gih = require("gulp-include-html"),
    pug = require('gulp-pug'), // Подключаем pug
    imagemin = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache = require('gulp-cache'), // Подключаем библиотеку кеширования
    spritesmith = require('gulp.spritesmith'), // Подключаем библиотеку создания спрайтов
    merge = require('merge-stream'), // Подключаем merge
    concat = require('gulp-concat'), // Подключаем библиотеку для объеденения файлов
    autoprefixer = require('gulp-autoprefixer');
//
// // uglify = require('gulp-uglify'), // Подключаем js-минификатор

let browserSync = require('browser-sync').create();

let pathBuild = './dist/',
    pathSrc = './src/',
    pathFonts = [
        pathSrc + 'fonts/**/*'
    ],
    tasks = ['img', 'js', 'sass', 'html', 'fontsDev', 'cleanCSSBuild', 'watch', 'browserSync',],
    tasksPug = ['img', 'js', 'sass', 'pug', 'fontsDev', 'cleanCSSBuild', 'watch', 'browserSync',];

gulp.task('sass', function () {
    return gulp.src(pathSrc + 'sass/**/*.+(sass|scss)')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
        .pipe(cleanCSS({compatibility: 'ie10'}))
        .pipe(gulp.dest(pathBuild + 'css'));
});

gulp.task('cleanCSSBuild', () => {
    return gulp.src(pathBuild + 'css/main.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(pathBuild + 'css/'))
});

gulp.task('pug', function () {
    gulp.src(pathSrc + 'pug/*.+(jade|pug)')
        .pipe(pug({pretty: '\t'}))
        .pipe(gulp.dest('dist/'))
});

gulp.task('js', function () {
    // return gulp.src(pathSrc + 'js/**/*.js')
    //     .pipe(gulp.dest('dist/js'));
    return gulp.src(pathSrc + 'js/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./dist/js/'));
});

// gulp.task('html', function () {
//     return gulp.src(pathSrc + 'html/*.html')
//         .pipe(gulp.dest('./dist'));
// });

gulp.task('html', function () {
    return gulp.src(pathSrc + 'html/**/*.html')
        .pipe(gih({
            // 'public': "./public/bizapp" + version,
            // 'version': version,
            baseDir: pathSrc + 'html/includes',
            ignore: '\/includes\/ '
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('img', function () {
    return gulp.src(pathSrc + 'img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('fontsDev', () => {
    return gulp.src(pathFonts)
        .pipe(gulp.dest(pathBuild + 'fonts'));
});

gulp.task('sprite', function () {
    let spriteData = gulp.src('src/sprite/*.png').pipe(spritesmith({
        imgName: '../img/sprite.png',
        cssName: 'sprite.scss'
    }));
    let imgStream = spriteData.img
        .pipe(gulp.dest('src/img/'));
    let cssStream = spriteData.css
        .pipe(gulp.dest('src/sprite/'));
    return merge(imgStream, cssStream);
});

gulp.task('browserSync', () => {
    browserSync.init({
        server: pathBuild
    });
});

gulp.task('watch', function () {
    gulp.watch('src/sass/**/*.+(sass|scss)', ['sass', 'cleanCSSBuild']).on('change', browserSync.reload);
    // gulp.watch('src/pug/**/*.+(jade|pug)', ['pug']);
    gulp.watch('src/html/*.html', ['html']).on('change', browserSync.reload);
    gulp.watch('src/html/includes/pages/*', ['html']).on('change', browserSync.reload);

    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/img/**/*', ['img']);
    gulp.watch('src/sprite/**/*.png', ['sprite']);
});

gulp.task('watchPug', function () {
    gulp.watch('src/sass/**/*.+(sass|scss)', ['sass', 'cleanCSSBuild']);
    gulp.watch('src/pug/**/*.+(jade|pug)', ['pug']);
    // gulp.watch('src/pug/**/*.html', ['html']);

    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/img/**/*', ['img']);
    gulp.watch('src/sprite/**/*.png', ['sprite']);
});

gulp.task('default', tasks);

gulp.task('only-pug', tasksPug);


